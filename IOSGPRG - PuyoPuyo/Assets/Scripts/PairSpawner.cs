﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PairSpawner : MonoBehaviour {

	public GameObject PairPrefab;

	private GameObject previewPair;
	private GameObject nextPair;

	private bool gameStarted = false;
	private Vector2 nextPairPos = new Vector2 (3, 10);
	private Vector2 previewPairPos = new Vector2 (9, 9);

	// Use this for initialization
	void Start () {
		SpawnNext ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SpawnNext(){
		if (!gameStarted) {
			gameStarted = true;

			nextPair =  Instantiate (PairPrefab, nextPairPos, Quaternion.identity);
			// Set preview puyo enabled property to false so it doesn't move or fall until the previous puyo has landed
			previewPair =  Instantiate (PairPrefab, previewPairPos, Quaternion.identity);
			previewPair.GetComponent<PuyoPair> ().enabled = false;

		} else {
			// Move preview puyo to where the spawn location for puyos are, and re-enable its puyo pair component
			previewPair.transform.localPosition = new Vector2 (3, 10);
			nextPair = previewPair;
			nextPair.GetComponent<PuyoPair> ().enabled = true;

			// Instantiate the next preview puyo
			previewPair =  Instantiate (PairPrefab, previewPairPos, Quaternion.identity);
			previewPair.GetComponent<PuyoPair> ().enabled = false;
		}
	}
}
