﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

	public static int gridWidth = 6;
	public static int gridHeight = 12;

	public static Transform[,] grid = new Transform[gridWidth, gridHeight];

	public GameObject gameOverText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//public bool CanClearMatch(){
	
	//}

	//private List<GameObject> FindMatch(Vector2 castDirection){
	//	List<GameObject> matchingPuyo = new List<GameObject> ();
	//	RaycastHit2D raycast = Physics2D.Raycast(transform.position, castDirection);
	//	// If raycast hits nothing, keep firing raycast, or if raycast hits a similar sprite
	//	while (raycast.collider != null && raycast.collider.GetComponent<SpriteRenderer> ().sprite == render.sprite) {
	//		matchingPuyo.Add (raycast.collider.gameObject);
	//		raycast = Physics2D.Raycast (raycast.collider.transform.position, castDirection);
	//	}
	//	return matchingPuyo;
	//}

	//public void ClearMatch(){
	//
	//}

	//public List<Puyo> GetNeighbors(Puyo currentPuyo)
	//{
	//}

	public void UpdateGrid(PuyoPair pair){
		// Iterate through all x per y to check if position is not null
		for (int y = 0; y < gridHeight; ++y) {
			for (int x = 0; x < gridWidth; ++x) {
				if (grid [x, y] != null) {
					if (grid [x, y].parent == pair.transform) {
						grid [x, y] = null;
					}
				}
			}
		}

		foreach (Transform child in pair.transform) {
			Vector2 pos = Round (child.position);

			// Only stores values to the grid if child's y position is less than grid height
			if (pos.y < gridHeight) {
				grid [(int)pos.x, (int)pos.y] = child;
			}
		}
	}

	public Transform GetTransformAtGridPos(Vector2 pos){
		// If y position is greater than grid height, return null. Otherwise, return grid position
		if (pos.y > gridHeight - 1) {
			return null;
		} else {
			return grid [(int)pos.x, (int)pos.y];
		}
	}

	public Vector2 Round(Vector2 pos){
		// Rounds vectors
		// Ex: (1.0001, 2) becomes (1, 2)
		return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
	}

	public bool CheckIfInsideGrid(Vector2 pos){
		// Puyo is inside the grid if all following conditions are true: If puyo's x position is greater than 0, if puyo's x position is less than grid width, and if puyo's y position is greater than 0.
		return ((int)pos.x >= 0 && (int)pos.x < gridWidth && (int)pos.y >= 0);
	}

	public bool CheckIfOnX(PuyoPair pair){
		for (int x = 0; x < gridWidth; ++x) {
			foreach (Transform child in pair.transform) {
				Vector2 pos = Round (child.position);
				if (pos.x == 3 && pos.y == 10) {
					return true;
				}
			}
		}
		return false;
	}

	//public IEnumerator FindNullGridPos(){
	//	// Iterate through all x per y to check if there are null grid positions
	//	for (int y = 0; y < gridHeight; ++y) {
	//		for (int x = 0; x < gridWidth; ++x) {
	//			// If grid position is null, start coroutine to shift tiles down
	//			if (grid [x, y].GetComponent<SpriteRenderer> ().sprite == null) {
	//				yield return StartCoroutine (ShiftPuyoDown (x, y));
	//				break;
	//			}
	//		}
	//	}
	//}

	//private IEnumerator ShiftPuyoDown(int x, int yStart, float shiftDelay){
	//
	//}

	public void GameOver(){
		Instantiate (gameOverText, new Vector2(2.5f, 6.0f), Quaternion.identity);
	}
}
