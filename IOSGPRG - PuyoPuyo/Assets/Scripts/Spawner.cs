﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public GameObject[] Groups;
	public GameObject puyoPair;

	private GameObject spawnedPuyo;
	private GameObject nextPuyo;

	// Use this for initialization
	void Start () {
		nextPuyo = SpawnNext ();
		nextPuyo.transform.SetParent (puyoPair.transform);
	}
	
	// Update is called once per frame
	void Update () {
	}

	public GameObject SpawnNext(){
		int i = Random.Range (0, Groups.Length);
		spawnedPuyo = Instantiate (Groups [i], this.transform.position, Quaternion.identity);

		return spawnedPuyo;
	}
}
