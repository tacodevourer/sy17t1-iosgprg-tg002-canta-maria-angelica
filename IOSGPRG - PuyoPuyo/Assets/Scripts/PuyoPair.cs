﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuyoPair : MonoBehaviour {

	private float fall = 0;
	public float fallSpeed = 1;

	public Spawner spawner;
	public PairSpawner pairSpawner;

	// Use this for initialization
	void Start () {
		spawner = GetComponent<Spawner> ();
		pairSpawner = GetComponent<PairSpawner> ();
	}
	
	// Update is called once per frame
	void Update () {
		CheckUserInput ();
	}

	void CheckUserInput(){
		// Movement
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			// Attempt to move puyo
			transform.position += new Vector3 (-1, 0, 0);

			// Check if move can be made, otherwise, revert the move.
			if (CheckIfValidPos ()) {
				FindObjectOfType<Grid> ().UpdateGrid (this);
			} else {
				transform.position += new Vector3 (1, 0, 0);
			}
		}
		else if(Input.GetKeyDown(KeyCode.RightArrow))
		{
			transform.position += new Vector3 (1, 0, 0);
			if (CheckIfValidPos ()) {
				FindObjectOfType<Grid> ().UpdateGrid (this);
			} else {
				transform.position += new Vector3 (-1, 0, 0);
			}
		}
		else if(Input.GetKeyDown(KeyCode.Space))
		{
			// Rotation
			transform.Rotate (0, 0, 90);
			if (CheckIfValidPos ()) {
				FindObjectOfType<Grid> ().UpdateGrid (this);
			} else {
				transform.Rotate (0, 0, -90);
			}
		}
		else if(Input.GetKeyDown(KeyCode.DownArrow) || Time.time - fall >= fallSpeed)
		{
			transform.position += new Vector3 (0, -1, 0);

			if (CheckIfValidPos ()) {
				FindObjectOfType<Grid> ().UpdateGrid (this);
			} else {
				transform.position += new Vector3 (0, 1, 0);

				if (FindObjectOfType<Grid> ().CheckIfOnX (this)) {
					FindObjectOfType<Grid> ().GameOver ();

					spawner.GetComponent<Spawner> ().enabled = false;
					pairSpawner.GetComponent<Spawner> ().enabled = false;
				}

				enabled = false;
				// Check if tile below is null. If null, shift tiles down


				FindObjectOfType<PairSpawner> ().SpawnNext ();
			}

			fall = Time.time;

		}
	}

	bool CheckIfValidPos(){
		// Iterate through children of pair and check if inside grid and if grid space to move into is empty. If not, return false.
		foreach(Transform child in transform){
			Vector2 pos = FindObjectOfType<Grid>().Round (child.position);
			if (FindObjectOfType<Grid> ().CheckIfInsideGrid (pos) == false) {
				return false;
			}
			if(FindObjectOfType<Grid>().GetTransformAtGridPos(pos) != null && (FindObjectOfType<Grid>().GetTransformAtGridPos(pos).parent != transform)){
				return false;
			}
		}

		return true;
	}
}
