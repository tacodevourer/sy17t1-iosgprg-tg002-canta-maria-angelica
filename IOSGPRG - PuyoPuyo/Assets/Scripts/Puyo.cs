﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puyo : MonoBehaviour {

	public enum PuyoColor
	{
		Blue,
		Green,
		Purple,
		Red,
		Yellow
	}

	public int Row { get; set; }
	public int Column { get; set; }
	public PuyoColor Color;

	// Use this for initialization
	void Start () {

		Debug.Log (this.Color);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
