﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour {

	public GameObject SpawnablePrefab;
	public GameObject ClonedPrefab;
	public List<GameObject> CloneList;
	public Transform SpawnPointEnd;
	public int MaxCloneNumber;
	public float GapSize;
	public float Min;
	public float Max;

	private float SpriteWidth;
	private float RandomY;

	// Use this for initialization
	void Start () {
		SpriteWidth = SpawnablePrefab.GetComponent<BoxCollider2D> ().size.x;
		CloneList = new List<GameObject>();
	}

	// Update is called once per frame
	void Update () {
		// If GroundSpawnPointBegin is located to the left of GroundSpawnPointEnd...
		// Spawn ground in between the two points and move GroundSpawnPointBegin to new location
		RandomY = Random.Range (Min, Max);
		if (transform.position.x < SpawnPointEnd.position.x) {
			transform.position = new Vector3 (transform.position.x + SpriteWidth + GapSize, RandomY, transform.position.z);
			Spawn ();
		}

		// Destroys oldest instantiated clone when the CloneList exceeds maximum clone number
		while (CloneList.Count > MaxCloneNumber) {
			if (CloneList [0] != null) {
				Destroy (CloneList [0].gameObject);
				CloneList.RemoveAt (0);
			}
		}
	}

	void Spawn(){
		ClonedPrefab = Instantiate (SpawnablePrefab, transform.position, Quaternion.identity);
		CloneList.Add (ClonedPrefab);
	}
}
