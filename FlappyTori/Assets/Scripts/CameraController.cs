﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public float x_offset;
	public float y;
	public float z;
	public Transform Player;

	// Use this for initialization
	void Start () {
		z = transform.position.z;
	}

	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(Player.position.x + x_offset, y, z);
	}
}