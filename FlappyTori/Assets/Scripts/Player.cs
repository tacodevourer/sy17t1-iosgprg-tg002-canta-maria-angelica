﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public float JumpForce;
	public float MoveSpeed;
	public int Score;

	private Rigidbody2D rb;
	private AnimationBehavior ab;

	// Use this for initialization
	void Start () 
	{
		// GetComponent searches and gets the list of components
		// The variable should be cached so that it only loads once on start
		rb = this.GetComponent<Rigidbody2D> ();
		ab = GetComponent<AnimationBehavior> ();
	}

	// Update is called once per frame
	void Update () 
	{
		// Sprite moves to the right
		transform.Translate (MoveSpeed * Time.deltaTime, 0, 0);

		if (Input.GetKeyDown (KeyCode.Space)) {		
			rb.gravityScale = 4;
			rb.velocity = new Vector2 (0, JumpForce);
		}
	} 

	void OnCollisionEnter2D(Collision2D coll)
	{
		// Function is called on collision

		// If collides with ground
		if (coll.gameObject.tag == "Ground") {
			JumpForce = 0;
			MoveSpeed = 0;

			ab.CurrentState = AnimationBehavior.AnimationState.Dead;
			Debug.Log (ab.CurrentState);
		}
	}

	void OnCollisionStay2D(Collision2D coll)
	{
		// Function is called as long as the object is still colliding
		Debug.Log ("Staying");
	}

	void OnCollisionExit2D(Collision2D coll)
	{
		// Function is called when object exits collision
		Debug.Log ("Exited");
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Trigger") {
			Score++;
		}
	}
}